import { head, filter } from 'ramda';

const initialState = {
  exampleData: null
};

module.exports = (state = initialState, action = {}) => {
  switch (action.type) {
    case 'TEST-ACTION-1': {
      const clonedState = { ...state };
      clonedState.exampleData = action.payload;
      return clonedState;
    }
    case 'TEST-ACTION-2': {
      const clonedState = { ...state };
      clonedState.exampleData = action.payload;
      return clonedState;
    }
  }
  return state;
};
