import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

// Library Imports
import { map } from 'lodash';
import { Dropdown } from 'semantic-ui-react'

import './port-dropdown.scss';

class PortDropdown extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: null
        };
    }

    generateAvailablePorts() {
        const { ports } = this.props;
        const availablePorts = [];

        if (ports) {
            map(ports, port => {
                availablePorts.push({
                    text: port.comName,
                    value: port.comName,
                    label: { color: 'orange', empty: true, circular: true }
                });
            });
        }
        return availablePorts;
    }

    handleChange = (e, { name, value }) => {
        this.setState({ selectedValue: value });
        const { handleReceiveSelectedPort } = this.props;
        if (handleReceiveSelectedPort) {
            handleReceiveSelectedPort(value);
        }
    }

    generateFieldTitle() {
        const { selectedValue } = this.state;
        if (selectedValue !== null) {
            return `${selectedValue.substring(0,18)}...`;
        }
        return 'Port';
    }


    render() {
        const availablePorts = this.generateAvailablePorts();

        return <Dropdown
            text={this.generateFieldTitle()}
            icon="usb"
            floating
            labeled
            button
            className="icon"
            onChange={this.handleChange}
            selection
            options={availablePorts}
        />

    }
}


export default PortDropdown
