// ASYNC METHODS

export const testAction1 = payload => {
  return dispatch => {
    dispatch({
      type: 'TEST-ACTION-1',
      payload
    });
  };
};
export const testAction2 = payload => {
  return async dispatch => {
    dispatch(testAction2Action(payload));
  };
};

// ACTIONS
export const testAction1Action = payload => {
  return {
    type: 'TEST-ACTION-1',
    payload
  };
};
export const testAction2Action = payload => {
  return {
    type: 'TEST-ACTION-2',
    payload
  };
};
