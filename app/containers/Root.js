// @flow
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { Switch, Route } from 'react-router';
/* Custom Page Container Imports (these are the visual layout components) */

import HomeContainer from './home/container';

export default class Root extends Component<Props> {
  render() {
    const { store, history } = this.props;
    return (
      <Provider store={store}>
          <ConnectedRouter history={history}>
              <Switch>
                  <Route component={HomeContainer} />
              </Switch>
          </ConnectedRouter>
      </Provider>
    );
  }
}
