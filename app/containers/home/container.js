import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {
  Button,
  Card,
  Container,
  Divider,
  Grid,
  Header,
  Label,
  Segment
} from 'semantic-ui-react';
import { head, map, filter } from 'lodash';
import { remote } from 'electron';
import { withRouter } from 'react-router-dom';
import SerialPort from 'serialport';

/* Components */
import PortDropdown from '../../components/port-dropdown/component';

/* Services */

/* Asset imports */

/* Rules */

/* Actions */


/* Themes */


/* Stylesheets */
import './styles.scss';

const currWindow = remote.getCurrentWindow();

class HomeContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
        debug: false,
        selectedPortName: null
    };
  }

  componentDidMount = async () => {
      this.getSerialPorts();
  };

  componentWillReceiveProps(nextProps) {

  }
  getSerialPorts = () => {
      const { filterPortsByManufacturer } = this.props;
      let availablePorts = null;

      SerialPort.list((err, ports) => {
          if (this.state.debug === true) { console.log('Serial Connection Manager :: listing serial ports => ', ports); }

          if (filterPortsByManufacturer !== null) {
              availablePorts = filter(ports, port => filterPortsByManufacturer === port.manufacturer);
              this.setState({ availablePorts });
              return true;
          }
          this.setState({ availablePorts: ports });
      });
  }

  handleReceiveSelectedPort = value => {
      if (this.state.debug === true) { console.log(`Serial Port Dropdown :: Callback triggered => ${value}`); }
      console.log(`Selected Port => ${value}`)
      this.setState({ selectedPortName: value });
  }

  handleClearSelectedPort = () => {
      if (this.state.debug === true) { console.log(`Serial Port Dropdown :: Callback triggered => ${value}`); }
      this.setState({ selectedPortName: null });
  }

  generateSerialPortsDropdown() {
      const { availablePorts, portStatus } = this.state;
      return (
        <PortDropdown
            ports={availablePorts}
            portStatus={portStatus}
            handleReceiveSelectedPort={this.handleReceiveSelectedPort}
        />
      );
  }

  generatePortStatusMessage() {
      const { selectedPortName } = this.state;
      if (selectedPortName !== null) {
          return (<Card.Description>You selected <strong>{ selectedPortName }</strong></Card.Description>);
      }
      return (<Card.Description>No port selected</Card.Description>);

  }

  generateApplicationBody() {
      return (
          <div className='home' style={{ display: 'flex', flexDirection: 'column', height: '100vh', width: '100vw', margin: '0px' }}>
              <div className={`bg light-pink-orange-gradient`}>
                  <Container fluid>
                      <Grid textAlign='center' verticalAlign='middle' columns={3}>
                          <Grid.Row>

                              <Grid.Column width={8}>
                                  <Header style={{ color: 'white', fontSize: '32px' }}>Test Application</Header>
                              </Grid.Column>

                          </Grid.Row>

                          <Grid.Row>
                              <Grid.Column width={8}>
                                <Card fluid>
                                  <Card.Content>
                                    <Card.Header>Serial Ports</Card.Header>
                                    <Card.Meta>Connect a serial device and select it from the list.</Card.Meta>
                                    <Card.Description>
                                        { this.generatePortStatusMessage() }
                                    </Card.Description>
                                  </Card.Content>
                                  <Card.Content extra>

                                      { this.generateSerialPortsDropdown() }

                                  </Card.Content>
                                  <Card.Content extra>
                                    <div className='ui two buttons'>
                                      <Button basic color='green' onClick={this.getSerialPorts}>
                                        Refresh
                                      </Button>
                                      <Button basic color='red' onClick={this.handleClearSelectedPort}>
                                        Clear
                                      </Button>
                                    </div>
                                  </Card.Content>
                                </Card>
                              </Grid.Column>

                          </Grid.Row>
                      </Grid>
                  </Container>
              </div>
          </div>
      );
  }

  determineBodyContent() {
      return this.generateApplicationBody();
  }

  render() {
      return this.determineBodyContent();
  }
}

const mapStateToProps = ({
  permissions,
  preferences,
  communication,
  user,
  configuration
}) => {
  return {
    permissions,
    preferences,
    communication,
    configuration,
    user
  };
};


export default withRouter(
  connect(
    mapStateToProps,
    null
  )(HomeContainer)
);
