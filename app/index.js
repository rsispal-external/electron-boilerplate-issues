import React from 'react';
import { render } from 'react-dom';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import createHistory from 'history/createBrowserHistory';
import {
  routerReducer,
  routerMiddleware
} from 'react-router-redux';
import ReduxThunk from 'redux-thunk';
import { AppContainer } from 'react-hot-loader';

import Root from './containers/Root';

import './app.global.css';
import 'semantic-ui-css/semantic.min.css';

/* Reducers */
import example from './reducers/example/reducers';

/* Create a history of your choosing (we're using a browser history in this case) */
const history = createHistory();

/* Build the middleware for intercepting and dispatching navigation actions */
const middleware = routerMiddleware(history);

const isProduction = process.env.NODE_ENV === 'production';

let store = null;

if (isProduction === true) {
  store = createStore(
    combineReducers({
      example,
      routerReducer
    }),
    compose(applyMiddleware(middleware, ReduxThunk))
  );
} else {
  store = createStore(
    combineReducers({
      example,
      routerReducer
    }),
    composeWithDevTools(applyMiddleware(middleware, ReduxThunk))
  );
}

render(
  <AppContainer>
    <Root store={store} history={history} />
  </AppContainer>,
  document.getElementById('root')
);

if (module.hot) {
  module.hot.accept('./containers/Root', () => {
    const NextRoot = require('./containers/Root'); // eslint-disable-line global-require
    render(
      <AppContainer>
        <NextRoot store={store} history={history} />
      </AppContainer>,
      document.getElementById('root')
    );
  });
}
